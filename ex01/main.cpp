/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/15 12:43:47 by aeddi             #+#    #+#             */
/*   Updated: 2015/01/15 14:18:26 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "span.hpp"
#include <iostream>
#include <cstdlib>

int main(void) {

	unsigned int	i;
	unsigned int	size = 1000000;
	unsigned int	ret;
	Span sp = Span(size);

	try {
		std::cout << std::endl << "Try to call shortestSpan() with an empty object, return: " << std::endl;
		ret = sp.shortestSpan();
		std::cout << ret << std::endl;
	}
	catch (std::exception & e) {
		std::cout <<  e.what() << std::endl;
	}

	try {
		std::cout << std::endl << "Try to call longestSpan() with an empty object, return: " << std::endl;
		ret = sp.longestSpan();
		std::cout << ret << std::endl;
	}
	catch (std::exception & e) {
		std::cout <<  e.what() << std::endl;
	}

	for (i = 0; i < size; i++) {
		sp.addNumber(rand());
	}

	try {
		std::cout << std::endl << "Try to call addNumber(42) with a full object will do an exception ?" << std::endl;
		sp.addNumber(42);
		std::cout << "Nope !" << std::endl;
	}
	catch (std::exception & e) {
		std::cout << "Yep !" << std::endl;
	}

	std::cout << std::endl << "shortestSpan() return: " << sp.shortestSpan() << std::endl;
	std::cout << "longestSpan() return: " << sp.longestSpan() << std::endl << std::endl;

	return 0;
}
