/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   span.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/15 11:43:50 by aeddi             #+#    #+#             */
/*   Updated: 2015/01/15 14:18:03 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "span.hpp"
#include <stdexcept>

				Span::Span(void) : _n(0) {
	return;
}


				Span::Span(unsigned int n) : _n(n) {
	return;
}

				Span::Span(Span const & src) {

	*this = src;

	return;
}

Span&			Span::operator=(Span const & rhs) {

	this->_n = rhs._n;
	this->_array = rhs._array;

	return *this;
}

				Span::~Span(void) {
	return;
}

void			Span::addNumber(int i) {
	
	if (this->_array.size() >= this->_n)
		throw std::exception();
	else
		this->_array.insert(i);
}

unsigned int	Span::shortestSpan(void) {

	if (this->_array.size() > 1) {
		std::multiset<int>::const_iterator	it = this->_array.begin();
		std::multiset<int>::const_iterator	it2 = this->_array.begin();
		std::multiset<int>::const_iterator	ite = this->_array.end();
		unsigned int						span = *ite - *it;

		it2++;
		for (; it2 != ite; it++, it2++) {
			if (static_cast<unsigned int>(*it2 - *it) < span)
				span = static_cast<unsigned int>(*it2 - *it);
		}
		return span;
	}
	throw std::exception();
}

unsigned int	Span::longestSpan(void) {

	if (this->_array.size() > 1)
		return *(--this->_array.end()) - *this->_array.begin();
	throw std::exception();
}
