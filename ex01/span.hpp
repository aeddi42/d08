/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   span.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/15 11:39:30 by aeddi             #+#    #+#             */
/*   Updated: 2015/01/15 12:20:46 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SPAN_HPP
# define SPAN_HPP

#include <set>

class Span {

	public:

							Span(unsigned int n);
							Span(Span const & src);
		Span&				operator=(Span const & rhs);
		virtual				~Span(void);

		void				addNumber(int i);
		unsigned int		shortestSpan(void);
		unsigned int		longestSpan(void);

	private:

							Span(void);

		unsigned int		_n;
		std::multiset<int>	_array;

};

#endif /* !SPAN_HPP */
