/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   easyfind.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/15 10:25:20 by aeddi             #+#    #+#             */
/*   Updated: 2015/01/15 11:35:28 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef EASYFIND_HPP
# define EASYFIND_HPP

template<typename T>
bool	easyfind(T a, int b) {

	typename T::const_iterator	it;
	typename T::const_iterator	ite = a.end();

	for (it = a.begin(); it != ite; it++) {
		if (*it == b)
			return true;
	}
	return false;
}

#endif /* !EASYFIND_HPP */
