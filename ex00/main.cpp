/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/15 11:17:41 by aeddi             #+#    #+#             */
/*   Updated: 2015/01/15 11:33:25 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <list>
#include <vector>
#include <iostream>
#include "easyfind.hpp"

int		main(void) {

	std::vector<int>	v(5, 0);
	std::list<int>		l;

	l.push_back(1);
	l.push_back(2);
	l.push_back(3);
	l.push_back(42);
	l.push_back(5);

	std::cout << std::boolalpha << std::endl;

	std::cout << "Vector contain 5x 0 / List contains 5 values: 1, 2, 3, 42, 5" << std::endl;
	std::cout << "easyfind(vector, 42) return " << easyfind(v, 42) << std::endl;
	std::cout << "easyfind(list, 42) return " << easyfind(l, 42) << std::endl << std::endl;

	std::cout << "Add 42 to the vector" << std::endl;
	v.insert(v.begin(), 42);
	std::cout << "easyfind(vector, 42) return " << easyfind(v, 42) << std::endl << std::endl;

	return 0;
}
