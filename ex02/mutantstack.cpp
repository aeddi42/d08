/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mutantstack.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/15 18:17:33 by aeddi             #+#    #+#             */
/*   Updated: 2015/01/15 19:59:47 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mutantstack.hpp"

template<typename T>
				MutantStack<T>::MutantStack(void) : std::stack<T>() {
	return;
}

template<typename T>
				MutantStack<T>::MutantStack(MutantStack<T> const & src) : std::stack<T>(src) {

	*this = src;
	return;
}

template<typename T>
MutantStack<T>&	MutantStack<T>::operator=(MutantStack<T> const & rhs) {

	std::stack<T>::operator=(rhs);
	return *this;
}

template<typename T>
				MutantStack<T>::~MutantStack(void) {
	return;
}

template<typename T>
MutantStack<T>::iterator::iterator(void) : current_(0), stack_() {
	return;
}

template<typename T>
MutantStack<T>::iterator::iterator(MutantStack<T> src) : current_(0), stack_(src) {
	return;
}

template<typename T>
MutantStack<T>::iterator::iterator(size_t size, MutantStack<T> src) : current_(size), stack_(src) {
	return;
}

template<typename T>
MutantStack<T>::iterator::iterator(iterator const & src) {

	*this = src;
	return;
}

template<typename T>
MutantStack<T>::iterator::~iterator(void) {
	return;
}

template<typename T>
typename MutantStack<T>::iterator& MutantStack<T>::iterator::operator=(MutantStack<T>::iterator const & rhs) {
	
	this->current_ = rhs.current_;
	this->stack_ = rhs.stack_;
	return *this;
}

template<typename T>
typename MutantStack<T>::iterator MutantStack<T>::iterator::begin(void) {
	return (stack_->top());
}

template<typename T>
typename MutantStack<T>::iterator MutantStack<T>::iterator::end(void) {
	return (stack_->top() - stack_->size());
}

template<typename T>
typename MutantStack<T>::iterator& MutantStack<T>::iterator::operator++(void) {

	++current_;
	return *this;
}

template<typename T>
typename MutantStack<T>::iterator& MutantStack<T>::iterator::operator--(void) {

	--current_;
	return *this;
}

template<typename T>
typename MutantStack<T>::iterator MutantStack<T>::iterator::operator++(int) {

	iterator	tmp(++current_, stack_);

	return tmp;
}

template<typename T>
typename MutantStack<T>::iterator MutantStack<T>::iterator::operator--(int) {

	iterator	tmp(--current_, stack_);

	return tmp;
}

template<typename T>
bool MutantStack<T>::iterator::operator!=(iterator const &r) {
	return (current_ != r.current_ || (stack_.top() - current_ != r.stack_.top() - r.current_)) ;
}

template<typename T>
T 	MutantStack<T>::iterator::operator*(void) {
	return *(&stack_.top() - current_);
}

template<typename T>
typename MutantStack<T>::iterator	MutantStack<T>::begin() {
	return iterator(*this);
}

template<typename T>
typename MutantStack<T>::iterator	MutantStack<T>::end() {
	return iterator(this->size(), *this);
}
