/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/15 14:26:09 by aeddi             #+#    #+#             */
/*   Updated: 2015/01/15 20:04:55 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mutantstack.cpp"
#include <iostream>

int main() {

	MutantStack<int> mstack;

	mstack.push(1);
	std::cout << "Push 1 on stack" << std::endl;
	mstack.push(2);
	std::cout << "Push 2 on stack" << std::endl;
	mstack.push(3);
	std::cout << "Push 3 on stack" << std::endl;

	std::cout << std::endl << "Top first elem: " << mstack.top() << std::endl;
	std::cout << "Size of stack: " << mstack.size() << std::endl << std::endl;

	std::cout << "Pop first elem" << std::endl;
	mstack.pop();
	std::cout << "Size of stack: " << mstack.size() << std::endl;

	mstack.push(4);
	std::cout << std::endl << "Push 4 on stack" << std::endl;
	mstack.push(5);
	std::cout << "Push 5 on stack" << std::endl;
	mstack.push(42); 
	std::cout << "Push 42 on stack" << std::endl;
	std::cout << "Size of stack: " << mstack.size() << std::endl << std::endl;

	MutantStack<int>::iterator it = mstack.begin();
	MutantStack<int>::iterator ite = mstack.end();

	std::cout << "Use it++" << std::endl;
	it++;
	std::cout << "Use it--" << std::endl;
	it--;
	std::cout << "Use ++it" << std::endl;
	++it;
	std::cout << "Use --it" << std::endl;
	--it;

	std::cout << std::endl << "While loop: " << std::endl;
	while (it != ite) {
		std::cout << "Iteration " << *it << std::endl;
		++it;
	}

	std::stack<int> s(mstack);

	return 0; 
}
