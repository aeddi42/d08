/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mutantstack.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/15 19:23:34 by aeddi             #+#    #+#             */
/*   Updated: 2015/01/15 19:42:09 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MUTANTSTACK_HPP
# define MUTANTSTACK_HPP

# include <stack>
# include <sstream>

template<typename T>
class MutantStack : public std::stack<T>
{

	public:

						MutantStack(void);
						MutantStack(MutantStack const & src);
		MutantStack&	operator=(MutantStack const & rhs);
		virtual			~MutantStack(void);


		class iterator {

			private:

								iterator(void);
				size_t			current_;
				MutantStack<T>	stack_;

			public:

								iterator(MutantStack<T> src);
								iterator(size_t size, MutantStack<T> src);
								iterator(iterator const & src);
				iterator&		operator=(iterator const & rhs);
								virtual ~iterator(void);

				iterator 		begin(void);
				iterator		end(void);

				iterator&		operator++(void);
				iterator&		operator--(void);
				iterator		operator++(int);
				iterator		operator--(int);
				bool			operator!=(iterator const & rhs);

				T 				operator*(void);

		};

		iterator			begin(void);
		iterator 			end(void);

};

#endif
